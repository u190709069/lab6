public class Rectangle {
    public int sideA;
    public int sideB;
    public Point top_left;
    public int corner1x;
    public int corner1y;
    public int corner2x;
    public int corner2y;
    public int corner3x;
    public int corner3y;

    public Rectangle(int sideA, int sideB, Point topLeft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.top_left = topLeft;
    }

    public int area() {
        return sideB * sideA;
    }

    public int perimeter() {
        return 2 * (sideA + sideB);

    }
    public int[][] Corners() {
        corner1x = top_left.xCordinate+sideB;
        corner1y = top_left.yCordinate;
        corner2x = top_left.xCordinate;
        corner2y = top_left.yCordinate-sideA;
        corner3x = top_left.xCordinate+sideB;
        corner3y = top_left.yCordinate-sideA;


        int[][] Corners = {{corner1x, corner1y}, {corner2x, corner2y}, {corner3x, corner3y}, {top_left.yCordinate, top_left.yCordinate}};
        return Corners;

    }
}
