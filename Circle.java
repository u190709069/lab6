public class Circle {
    public int radius;
    public Point center;

    public double area() {
        return Math.PI*radius*radius;
    }
    public Circle (int radius,Point center) {
        this.radius=radius;
        this.center=center;

    }
    public double perimeter(){
        return Math.PI*2*radius;
    }
    public boolean intersect(Circle b,Circle a){
        double c1c2;
        c1c2 = Math.sqrt(Math.pow((a.center.xCordinate - b.center.xCordinate),2)+ Math.pow((a.center.yCordinate- b.center.xCordinate),2));
        return !(c1c2 < a.radius + b.radius) || (Math.abs(a.radius - b.radius) > c1c2);
    }

}

