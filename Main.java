import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(8,5,new Point(9,6));
        System.out.println("area of the rectangle:" + rectangle.area());
        System.out.println("perimeter of the rectangle:" + rectangle.perimeter());
        System.out.println(Arrays.deepToString(rectangle.Corners()));
        System.out.println(" ");
        Circle circle_a = new Circle(2,new Point(5,8));
        System.out.println("area of the circle:" + circle_a.area());
        System.out.println("perimeter of the circle:" + circle_a.perimeter());
        Circle circle_b = new Circle(2,new Point(5,8));
        System.out.println(circle_a.intersect(circle_a,circle_b));

    }
}
